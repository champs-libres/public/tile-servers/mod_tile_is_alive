from flask import (Flask, json)
import mercantile
import random
import requests

app = Flask(__name__)

# coordinates (raw bbox from england to turkey)
min_x = -3.10
max_x = 27.70
min_y = 37.18
max_y = 54.0

# zoom levels
zooms = [17, 18, 19, 20]

# server
host = "http://localhost/tiles"

@app.errorhandler(500)
def handle_error_500(error):
    print(error)
    return error, 500

@app.route('/')
def is_alive():
    # get a random point between min and max
    x = random.randrange(min_x * 10000, max_x * 10000) / 10000
    y = random.randrange(min_y * 10000, max_y * 10000) / 10000
    z = random.choice(zooms)
    # get the tile coordinates for this point
    tile = mercantile.tile(x, y, z)
    tile_url = "{}/{}/{}/{}.png".format(host, tile.z, tile.x, tile.y)
    r = requests.head(tile_url)

    message = { "tile_url": tile_url, "response_time": r.elapsed.total_seconds(), 'response_host': r.status_code }

    # handle error
    if r.status_code != 200:
        return (message, 500)
    else:
        return (message, 200)


    return 'Hello, World!'
